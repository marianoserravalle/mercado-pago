import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http';
import { PUBLISHABLE_KEY,BACKEND_ENDPOINT } from "./config";
/*
  Generated class for the MercadoPagoProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class MercadoPagoProvider {

  doSubmit:boolean = false;
  cardNumberElement = null;
  payElement = null;
  formElement = null;
  paymentMethodId = null;
  token = null;
  email:string = null;
  docNumber = null;
  data:any = {};

  constructor(public http: HTTP) {
    ///////////////////////////////////////////
    
    /* APP_USR-2ebf745b-fca0-461f-be6f-17d155e8bdd9 */
    /* TEST-4291a508-c810-402c-864d-81b903dc57cb */
    console.log(PUBLISHABLE_KEY);
    this.initialize(PUBLISHABLE_KEY);
  }



    //Envía el token a tus servidores
    //this.addEvent(this.payElement, 'submit', this.doPay);

  initialize(publishableKey:string){
    Mercadopago.setPublishableKey(publishableKey);
    Mercadopago.getIdentificationTypes();
  }
  

  setCardNumberElement(element){
    this.cardNumberElement = element;
  }

  setPayButtonElement(element){
    this.payElement = element;
  }

  setPayFormElement(formElement){
    this.formElement = formElement;
  }


  getBin() {
    var ccNumber: any = this.cardNumberElement;
    return ccNumber.value.replace(/[ .-]/g, '').slice(0, 6);
  }

  guessingPaymentMethod(eventType:string) {
    let bin = this.getBin();

    if (eventType == "keyup") {
      if (bin.length >= 6) {

        Mercadopago.getPaymentMethod({
          "bin": bin
        }, this.setPaymentMethodInfo.bind(this));
      }
    } else {
      setTimeout( () => {

        if (bin.length >= 6) {
        console.log('change 6')

          Mercadopago.getPaymentMethod({
            "bin": bin
          }, this.setPaymentMethodInfo.bind(this));
        }
      }, 100);
    }
  }

  setPaymentMethodInfo(status, response) {
    console.log("setPaymentMethodInfo");
    console.log(status);
    console.log(JSON.stringify(response));

    if (status == 200) {
      // do somethings ex: show logo of the payment method
      
      
      //console.log("setPaymentMethodInfoBefore");
      let form:HTMLElement = this.formElement;

      if (this.paymentMethodId == null) {

        //console.log("setPaymentMethodInfonew");
        try{

          //var paymentMethod = document.createElement('input');
          //console.log("setPaymentMethodInfo2");
          //paymentMethod.setAttribute('name', "paymentMethodId");
          this.paymentMethodId = response[0].id;
          //paymentMethod.setAttribute('type', "hidden");
          //paymentMethod.setAttribute('value', response[0].id);
  
          //form[0].appendChild(paymentMethod);
  
          //var paymentMethodId = document.getElementsByName("paymentMethodId");
        }catch(error){
          console.log(error);
        }
       

      } else {
        console.log("setPaymentMethodInfoElse");

        //let tag: any = document.querySelector("input[name=paymentMethodId]")
        this.paymentMethodId = response[0].id;
      }
    }
  }

  doPay(form) {
    try{

      //if (!this.doSubmit) {
        
        console.log("entroo");
        Mercadopago.createToken(form, this.sdkResponseHandler.bind(this)); // The  function "sdkResponseHandler" is defined below
  
        return false;
    }catch(err){
      console.log(err);
    }
    //}
  }

  sdkResponseHandler(status, response) {

    console.log("sdkResponseHandler");
    console.log(status);
    console.log(response);

    if (status != 200 && status != 201) {
      alert("Por favor, verifique los datos ingresados");
    } else {

      this.token = response.id;
      console.log(this.token);
      //var card = document.createElement('input');
      //card.setAttribute('name', "token");
      //card.setAttribute('type', "hidden");
      //card.setAttribute('value', response.id);
      //form.appendChild(card);
      // this.doSubmit = true;
     /*  alert(JSON.stringify(response)); */
      //form.submit();

      // TODO no funciono agarrar el mail by name, seguir probando y sino por id.

      // var paymentMethodId = document.getElementsByName("paymentMethodId");
      // let payment: any = paymentMethodId[0];

      // var mail = document.querySelectorAll("#email input");
      // let email: any = mail[0];

      // var docu = document.querySelectorAll("#docNumber input");
      // let dni: any = docu[0];
      console.log('DATA SDKHANDLER BEFORE ');
      try{
        let data = {
          token: this.token,
          email: this.email,
          docNumber: this.docNumber,
          data: this.data
        }
     
     

        console.log(JSON.stringify(this.data));
      this.sendToken(BACKEND_ENDPOINT,data)
          .then((response)=>{
            console.log(JSON.stringify(response));
          })
          .catch((err)=>{
            console.log(JSON.stringify(err));
          });

        }catch(err){
          console.log(err)
        }
      /*let loading = loadC.create({ content: 'Efectuando el pago...' });
      loading.present();
      

      userProviderVar.sendToken(response.id, payment.value, email.value, dni.value)
        .then((data: any) => {
          if (data.state == 'success') {
            loading.dismiss();
          
            navCtrl.pop();
            
            let toast = this.toastCtrl.create({
              message: 'Se ha efectuado el pago correctamente',
              duration: 3000,
              position: 'middle'
            });

            toast.present();

          } else {
            loading.dismiss();

            let toast = this.toastCtrl.create({
              message: 'No se ha podido efectuar el pago, intente mas tarde',
              duration: 3000,
              position: 'middle'
            });

            toast.present();


          }


        })
        .catch(err => console.log('Error launching dialer', err));
      //this.enviarToken(response);*/
    }
  }

  setEmail(email:string){
    this.email = email;
  }

  setDocNumber(docNumber){
    this.docNumber = docNumber;

  }
  
  setData(data:any){
    this.data = data;
  }

  sendToken(endpoint:string, data:any){
    let promise = new Promise((resolve,reject)=>{

      this.http.post(endpoint,data,{})
        .then((response)=>{
          resolve(response);
        })
        .catch((err)=>{
          resolve(err);
        });
      }
    );

    return promise;

  }

}

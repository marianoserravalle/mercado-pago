import { Component, ViewChild,ElementRef,Input } from '@angular/core';
import { MercadoPagoProvider } from "./mercado-pago.provider";
import { CUSTOM_IMAGE } from "./config";

@Component({
  selector: 'mercado-pago',
  templateUrl: 'mercado-pago.html'
})
export class MercadoPagoComponent {
  @ViewChild('cardNumber') cardNumber:ElementRef;
  @ViewChild('payButton') payButton;
  @ViewChild('payForm') payForm; 
  @ViewChild('email') email; 
  @ViewChild('docNumber') docNumber; 
  @Input() data:any = {};
 
  image:string = null;



  constructor(public mercadoPagoProvider:MercadoPagoProvider) {
    this.image = CUSTOM_IMAGE;
  }

  ngAfterViewInit(){
    this.mercadoPagoProvider.setCardNumberElement(this.cardNumber);
    this.mercadoPagoProvider.setPayButtonElement(this.payButton);
    this.mercadoPagoProvider.setPayFormElement(this.payForm);

  }

  submitForm(){
    this.mercadoPagoProvider.setEmail(this.email);
    this.mercadoPagoProvider.setDocNumber(this.email);
    this.mercadoPagoProvider.setData(this.data);
    
    let form = document.getElementById("payForm");
    this.mercadoPagoProvider.doPay(form);
  }




}
